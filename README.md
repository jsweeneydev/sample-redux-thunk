Sample project using redux-thunk to handle asynchronous actions.



Mainly made it as a personal example of how to handle thunk and also how to handle form submission with Express (because I always seem to forget how to handle it).



Setup:

```sh
npm install
```

To run:

```sh
node .
```

To build the client-side code (doesn't include middleware!)

```sh
npm run build
```

