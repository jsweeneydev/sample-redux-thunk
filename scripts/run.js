const express = require('express')
const http = require('http')
const webpack = require('webpack')
const setupWebpackMiddleware = require('webpack-dev-middleware')
const path = require('path')
const reload = require('reload')
const watch = require('watch')

const buildConfig = require('./config.js')
const generatePage = require('./generate-page.js')

const port = buildConfig.port

const notes = [
  // {
  //   title: 'Grocery Store',
  //   note: 'Quart of milk\nLoaf of bread\nStick of butter'
  // }
]

const app = express()
// app.use(require('body-parser').json())
app.use(require('body-parser').urlencoded({ extended: false }))
const reloadServer = reload(app, { verbose: true })
const webpackMiddleware = setupWebpackMiddleware(webpack({
  ...buildConfig.webpack,
  mode: 'development',
  output: {
    path: '/',
    filename: 'script.js',
    devtoolModuleFilenameTemplate: info => info.resourcePath
  }
}))
app.use(webpackMiddleware)
app.get('/', (req, res) => {
  res.status(200).send(generatePage({ includeReload: true }))
})
;[
  'style.css',
  'favicon.png',
  'favicon.ico',
  'submitSuccessfulPage.html'
].forEach(file => {
  app.get(`/${file}`, (req, res) => {
    res.status(200).sendFile(path.resolve(`./src/${file}`))
  })
})
app.use('/static', express.static('./src/static'))

app.post('/add-note', (req, res) => {
  // Too lazy to check for errors :)
  notes.unshift({
    title: req.body.title,
    note: req.body.note
  })
  res.status(200).sendFile(path.resolve('./src/submitSuccessfulPage.html'))
})

app.get('/get-notes', (req, res) => {
  res
    .status(200)
    .append('Content-Type', 'application/json')
    .send(JSON.stringify(notes))
})

const server = http.createServer(app)
app.set('port', port)
server.listen(app.get('port'), () => {
  console.log(`Web server listening on port ${port}`)
})
// Reload the page if the script changes
let lastScript = null
setInterval(() => {
  const script = webpackMiddleware.fileSystem.data['script.js']
  if (lastScript !== script) {
    lastScript = script
    reloadServer.reload()
  }
}, 50)
// Reload the page if any non-script-related files change
watch.createMonitor('./src/', monitor => {
  monitor.on('changed', f => {
    if (!(/.*.jsx?$/.test(f))) {
      reloadServer.reload()
    }
  })
})
