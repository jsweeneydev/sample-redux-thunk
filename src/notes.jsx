import React from 'react'

class Notes extends React.Component {
  render () {
    const {
      notes
    } = this.props

    return <div className='existing-notes'>
      <span>{(() => notes.map(({ title, note }, index) =>
        <div key={index}>
          <span>{title}</span>
          <textarea disabled value={note} rows='10' />
        </div>
      ))()}</span>

    </div>
  }
}
export default Notes
