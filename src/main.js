import React from 'react'
import { render } from 'react-dom'
import { Provider, connect } from 'react-redux'
// import * as redux from 'redux'
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

import mainReducer from './reducers/mainReducer.js'
import Main from './main.jsx'

import { fetchNotes, postNote } from './actions.js'

const store = createStore(
  mainReducer,
  compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : state => state
  )
)

const ConnectedMain = connect(
  // Map state to props
  state => {
    return state
  },
  // Map dispatch to props
  dispatch => ({
    onNotePosted: note => {
      store.dispatch(postNote(note))
        .then((a) => console.log(a))
    }
  })
)(Main)

render(
  <Provider store={store}>
    <ConnectedMain />
  </Provider>,
  document.querySelector('.app-wrapper')
)

store.dispatch(fetchNotes())
  .then((a) => console.log(a))
