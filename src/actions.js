export const NOTES_REQUESTED = 'NOTES_REQUESTED'

export function handleNotesRequested () {
  return {
    type: NOTES_REQUESTED
  }
}

export const NOTES_LOADED = 'NOTES_LOADED'

export function handleNotesLoaded (notes) {
  return {
    type: NOTES_LOADED,
    payload: { notes }
  }
}

export const NOTES_FAILED = 'NOTES_FAILED'

export function handleNotesFailed (error) {
  return {
    type: NOTES_FAILED,
    payload: { error }
  }
}

export function fetchNotes () {
  return dispatch => {
    dispatch(handleNotesRequested())
    return window.fetch('get-notes')
      .then(
        response => response.json(),
        error => dispatch(handleNotesFailed(error))
      )
      .then(json => dispatch(handleNotesLoaded(json)))
  }
}

// ---

export const NOTE_SENT = 'NOTE_SENT'

export function handleNoteSent (note) {
  return {
    type: NOTE_SENT,
    payload: note
  }
}

export const NOTE_POSTED = 'NOTE_POSTED'

export function handleNotePosted (notes) {
  return {
    type: NOTE_POSTED
  }
}

export const NOTE_POST_FAILED = 'NOTE_POST_FAILED'

export function handleNotePostFailed (error) {
  return {
    type: NOTE_POST_FAILED,
    payload: { error }
  }
}

export function postNote (noteJson) {
  return dispatch => {
    const formData = new URLSearchParams()
    Object.entries(noteJson).forEach(([key, value]) => {
      formData.append(key, value)
    })

    dispatch(handleNoteSent(noteJson))
    return window.fetch('add-note', {
      method: 'POST',
      mode: 'no-cors',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: formData
    })
      .then(
        response => dispatch(fetchNotes()),
        error => dispatch(handleNotesFailed(error))
      )
  }
}
