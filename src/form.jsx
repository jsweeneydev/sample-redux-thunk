import React from 'react'
class Form extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      title: '',
      note: ''
    }

    ;[
      'updateTitle',
      'updateNote',
      'submitNote'
    ].forEach(f => {
      this[f] = this[f].bind(this)
    })
  }
  updateTitle (event) {
    this.setState({ title: event.target.value })
  }
  updateNote (event) {
    this.setState({ note: event.target.value })
  }
  submitNote (event) {
    const { onNotePosted } = this.props
    console.log(this.props)
    event.preventDefault()
    console.log(`Submitting ${JSON.stringify(this.state, null, 2)}`)
    onNotePosted({
      title: this.state.title,
      note: this.state.note
    })
  }
  render () {
    // const {
    //   todos,
    //   onChecked
    // } = this.props

    return <form onSubmit={this.submitNote} method='post' action='add-note'>
      <label>
        Title
        <input
          type='text'
          value={this.state.title}
          onChange={this.updateTitle}
          name='title'
        />
      </label>
      <label>
        Note
        <textarea rows='10'
          value={this.state.note}
          onChange={this.updateNote}
          id='note'
          name='note'
        />
      </label>
      <input type='submit' />
    </form>
  }
}
export default Form
