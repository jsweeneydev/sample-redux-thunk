import { state as defaultState } from '../default.js'

import {
  NOTES_REQUESTED,
  NOTES_LOADED,
  NOTES_FAILED
} from '../actions.js'

const mainReducer = (state = defaultState, action) => {
  const { type, payload } = action
  switch (type) {
    case NOTES_REQUESTED:
      return {
        ...state,
        notesRequested: true
      }
    case NOTES_LOADED:
      return {
        ...state,
        notesRequested: false,
        notes: payload.notes
      }
    case NOTES_FAILED:
      return {
        ...state,
        notesRequested: false,
        lastError: payload.error
      }
  }
  return state
}

export default mainReducer
