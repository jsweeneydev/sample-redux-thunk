import React from 'react'
import Form from './form.jsx'
import Notes from './notes.jsx'

class Main extends React.Component {
  render () {
    const {
      notes,
      onNotePosted
    } = this.props

    return <div>
      <h1>Notes</h1>
      <Form
        onNotePosted={onNotePosted}
      />
      <Notes
        notes={notes}
      />
    </div>
  }
}
export default Main
